package Runners;

import org.junit.runner.RunWith;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;

@RunWith(Cucumber.class)
@CucumberOptions(
    features = "C:/Users/cecilia/eclipse-workspace/PruebaAutomationUISeniorLead/src/test/resources/features/opencart.feature",
    glue = "Steps",
    plugin = { "pretty", "html:target/cucumber-reports"},
    monochrome = true
)

public class runner {

}