package Pages;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;

public class HomePageOpenCart extends BasePage{
	
	public HomePageOpenCart() {
		super(driver);
	}

	By input_search = By.name("search");
	By button_search = By.xpath("//*[@id=\"search\"]/span/button");
	By img_product = By.xpath("//*[@id=\"content\"]/div[3]/div/div/div[1]/a/img");
	By button_add_cart = By.id("button-cart");
	By button_cart_total = By.id("cart-total");
	By link_view_cart = By.xpath("//*[@id=\"cart\"]/ul/li[2]/div/p/a[1]");

	public void openUrlOpencart() {
		driver.get("http://opencart.abstracta.us/");
		driver.manage().window().maximize();
	}
	
	public void searchProduct(String writeproduct) {
		driver.findElement(input_search).sendKeys(writeproduct, Keys.ENTER);
	}
	
	public void selectProduct() {
		driver.findElement(img_product).click();
	}
	
	public void addToCart() {
		driver.findElement(button_add_cart).click();
	}
	
	public void buttonCart() {
		driver.findElement(button_cart_total).click();
	}
	
	public void ViewCart() {
		driver.findElement(link_view_cart).click();
	}

}
