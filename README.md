# Ejercicio práctico: UI Automation

(Senior / Lead)

Crear ejercicio de automatización UI con Selenium o WebDriverIO que realice los siguientes pasos:

```
1. Abrir web de http://opencart.abstracta.us/ (Puede utilizar cualquier navegador).
2. En la barra de búsqueda, ingresar el producto “iPhone” y buscar.
3. Seleccionar la primera opción que aparezca.
4. Agregar el producto al carrito de compras.
5. Ingresar al botón del carrito de compras.
6. Presionar "View Cart".
7. Validar que el iPhone seleccionado se encuentre en el carrito de compras.
8. Remover el iPhone del carrito de compras.
9. Validar que el iPhone ya no se encuentre en el carrito de compras.
10. Realizar capturas de pantalla en los pasos del test que se consideren necesarios.
11. Realizar los scripts de automatización con BDD (Cucumber) para ello utilizar la siguiente estructura:
- Feature: Opencart Test
- Scenario: Envío el producto iPhone al carrito de compras para luego removerlo y validar que no se encuentre en el carrito.
- Agregar los pasos de la automatización con: Given, And y When.
12. Utilizar el patrón de diseño Page Object.
13. Subir proyecto a un pipeline de integración continua (Jenkins, Gitlab CI, etc) y ejecutar el job.

```

Se valorará:
```
- Creación de un framework de automatización. Ejemplo: Selenium: Maven, TestNG, Cucumber, etc. WebDriverIO: Mocha, Chai, Jasmine, Cucumber, etc.
- Buenas prácticas de programación (utilización de selectores, assertions, tiempos de espera, etc.)
- Reporte de ejecución del pipeline ejecutado.
```

Durante la demo de revisión con el líder técnico se solicitará:

```
- Ejecutar la automatización con la búsqueda del producto.
- Explicar la elección de selectores, assertions, librerías instaladas y patrón de diseño utilizado, entre otros.
```

El entregable debe ser un repositorio en GitLab o GitHub (público), conteniendo la automatización que resuelve el problema. 

